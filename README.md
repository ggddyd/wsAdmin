# 1.项目介绍

> 该项目采用 vue3.0+ts+elementPlus+vite 搭建

# 2.运行命令

**装包：**

> yarn

**运行代码：**

> yarn dev

**分环境打包：**

> 测试环境打包 ：yarn build:test

> 生产环境打包：yarn build:pro

**eslint 代码检查配置：**

> eslint 代码检查：yarn lint

> eslint 代码检查修复：yarn fix

**代码格式化**：

当我们对代码进行 commit 操作的时候，就会执行命令，对代码进行格式化，然后再提交

> 代码格式化：yarn format

**stylelint 检查**：

可格式化 css 代码，检查 css 语法错误与不合理的写法，指定 css 书写顺序等

> 检查 css 代码: yarn lint:style

# 3.提交格式限制

```
'feat',//新特性、新功能
'fix',//修改bug
'docs',//文档修改
'style',//代码格式修改, 注意不是 css 修改
'refactor',//代码重构
'perf',//优化相关，比如提升性能、体验
'test',//测试用例修改
'chore',//其他修改, 比如改变构建流程、或者增加依赖库、工具等
'revert',//回滚到上一个版本
'build',//编译相关的修改，例如发布版本、对项目构建或者依赖的改动
```

**当我们 commit 提交信息时，就不能再随意写了，必须是 git commit -m 'fix: xxx' 符合类型的才可以，需要注意的是类型的后面需要用英文的 :，并且冒号后面是需要空一格的，这个是不能省略的**

# 4.项目文档

## 4.1.账号密码

> 菜单的权限:
>
> 超级管理员账号:admin atguigu123 拥有全部的菜单、按钮的权限
>
> 普通账号 硅谷 333 111111 不包含权限管理模块、按钮的权限并非全部按钮

## 4.2 接口文档

服务器域名:http://sph-api.atguigu.cn

swagger 文档:

http://139.198.104.58:8209/swagger-ui.html

http://139.198.104.58:8212/swagger-ui.html#/

echarts:国内镜像网站

https://www.isqqw.com/echarts-doc/zh/option.html#title

http://datav.aliyun.com/portal/school/atlas/area_selector

# 5.项目截图

![](https://pic.imgdb.cn/item/65649fe0c458853aefe573d5.jpg)

![](https://pic.imgdb.cn/item/65649ff6c458853aefe5d5f3.jpg)
