import { createApp } from "vue";
import ElementPlus from "element-plus";
import App from "@/App.vue";
//引入全局样式
import "@/styles/index.scss";
import "element-plus/dist/index.css";
//引入animat.css
import "animate.css";
//配置element-plus的国际化，默认是英文
//@ts-expect-error忽略当前文件ts类型的检测否则有红色提示(打包会失败)
import zhCn from "element-plus/dist/locale/zh-cn.mjs";
//导入svg
import "virtual:svg-icons-register";
//引入自定义组件
import vueComponent from "@/utils/vueComponent.ts";
//引入路由
import router from "@/router/index.ts";
//引入pinia仓库
import pinia from "@/store/index.ts";
//鉴权文件
import "@/permisstion/permisstion";
//获取应用实例对象
const app = createApp(App);
app.use(vueComponent);
//注册路由
app.use(router);
//注册仓库
app.use(pinia);
//使用element-plus插件
app.use(ElementPlus, {
  locale: zhCn,
});

app.mount("#app");
