const login = () => import("@/views/login/index.vue");
const home = () => import("@/views/home/index.vue");
const a404 = () => import("@/views/404/index.vue");
const layout = () => import("@/layout/index.vue");
const screen = () => import("@/views/screen/index.vue");

//权限管理
const userManagement = () => import("@/views/acl/userManagement/index.vue");
const roleMangement = () => import("@/views/acl/roleMangement/index.vue");
const menuManagement = () => import("@/views/acl/menuManagement/index.vue");
//商品管理
const trademark = () => import("@/views/products/trademark/index.vue");
const attr = () => import("@/views/products/attr/index.vue");
const Sku = () => import("@/views/products/Sku/index.vue");
const Spu = () => import("@/views/products/Spu/index.vue");
//定义常量公共路由,全部用户都能访问的
const constantRoute = [
  {
    //登录路由
    path: "/login",
    component: login,
    name: "login",
    meta: {
      title: "登录",
      hidden: true,
      icon: "Promotion",
    },
  },
  {
    //登录成功展示数据路由
    path: "/",
    component: layout,
    name: "layout",
    meta: {
      title: "",
      icon: "",
    },
    redirect: "/home",
    children: [
      {
        path: "/home",
        name: "home",
        component: home,
        meta: {
          title: "首页",
          icon: "HomeFilled",
        },
      },
    ],
  },
  {
    path: "/screen",
    name: "screen",
    component: screen,
    meta: {
      title: "数据大屏",

      icon: "DataAnalysis",
    },
  },

  {
    //404路由
    path: "/404",
    component: a404,
    name: "404",
    meta: {
      title: "404",
      hidden: true,
      icon: "",
    },
  },
];

//异步路由
//异步路由
const asycRoute = [
  {
    path: "/acl",
    name: "Acl",
    component: layout,
    redirect: "/acl/user",
    meta: {
      title: "权限管理",
      icon: "Lock",
    },
    children: [
      {
        path: "/acl/user",
        name: "User",
        component: userManagement,
        meta: {
          title: "用户管理",
          icon: "UserFilled",
        },
      },
      {
        path: "/acl/role",
        name: "Role",
        component: roleMangement,
        meta: {
          title: "角色管理",
          icon: "Tools",
        },
      },
      {
        path: "/acl/menu",
        name: "Permission",
        component: menuManagement,
        meta: {
          title: "菜单管理",
          icon: "Menu",
        },
      },
    ],
  },
  {
    path: "/product",
    component: layout,
    name: "Product",
    meta: {
      title: "商品管理",
      icon: "Goods",
    },
    redirect: "/product/trademark",
    children: [
      {
        path: "/product/trademark",
        component: trademark,
        name: "Trademark",
        meta: {
          title: "品牌管理",
          icon: "ShoppingCartFull",
        },
      },
      {
        path: "/product/attr",
        component: attr,
        name: "Attr",
        meta: {
          title: "属性管理",
          icon: "ChromeFilled",
        },
      },
      {
        path: "/product/spu",
        component: Spu,
        name: "Spu",
        meta: {
          title: "SPU管理",
          icon: "Calendar",
        },
      },
      {
        path: "/product/sku",
        component: Sku,
        name: "Sku",
        meta: {
          title: "SKU管理",
          icon: "Orange",
        },
      },
    ],
  },
];

//任意路由
const anyRoute = [
  {
    //任意路由到404
    path: "/:pathMatch(.*)*",
    redirect: "/404",
    name: "Any",
    meta: {
      title: "任意页面",
      hidden: true,
      icon: "",
    },
  },
];
export { constantRoute, asycRoute, anyRoute };
