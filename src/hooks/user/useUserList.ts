// userHooks.ts
import { ref } from "vue";
import api from "@/api/index";

export function useUserList() {
  //列表数据
  const tableData = ref<any>([]);
  //默认页码
  const pageNo = ref<number>(1);
  //一页展示几条数据
  const pageSize = ref<number>(5);
  //用户总个数
  const total = ref<number>(0);
  //定义响应式数据:收集用户输入进来的关键字
  const keyword = ref<string>("");
  const getHasUser = async () => {
    try {
      const response = await api.aclUser.reqUserInfo(
        pageNo.value,
        pageSize.value,
        keyword.value
      );
      tableData.value = response.data.records;
      total.value = response.data.total;
    } catch (error) {
      console.error("Error fetching user data:", error);
    }
  };

  const updatePageNo = (newPageNo: number) => {
    pageNo.value = newPageNo;
    getHasUser();
  };

  const updatePageSize = (newPageSize: number) => {
    pageSize.value = newPageSize;
    getHasUser();
  };
  return {
    tableData,
    pageNo,
    pageSize,
    total,
    keyword,
    getHasUser,
    updatePageNo,
    updatePageSize,
  };
}
