//路由鉴权文件
import router from "@/router";
import { start, close } from "./nprogress";
//获取token判断 用户是否登录成功
import pinia from "@/store/index";
import userInfoStore from "@/store/modules/user";
import { extractNames } from "@/utils/permissionRoute";
import { constantRoute, anyRoute } from "@/router/route";
const userStore = userInfoStore(pinia);
import StoragePlugin from "@/plugins/storagePlugin";
const storagePlugin = new StoragePlugin();

//访问某个路由之前的守卫
// ● to: 表示即将要进入的目标路由对象。它包含了目标路由的信息，如路由的路径、参数、query等。
// ● from: 表示当前导航正要离开的路由对象。它包含了当前路由的信息，如当前路由的路径、参数、query等。
// ● next: 是一个函数，用于确保路由守卫能够执行完毕并且继续导航。在路由守卫中，必须调用next方法来确保路由能够继续进行导航。可以通过传递参数到next方法来控制导航的行为，比如next()表示继续导航，next(false)表示中断当前导航，next('/login')表示跳转到指定的路由路径等。
router.beforeEach(async (to: any, from: any, next: any) => {
  const token = storagePlugin.get("TOKEN");
  const username = userStore.userInfos.name;

  start();

  // 情况1：用户登录不能访问login[指向首页],其余路由能访问
  if (token) {
    // 如果已登录，从 /login 重定向到主页
    if (to.path === "/login") {
      return next({ path: "/" });
    }
    // 如果用户信息存在，允许访问路由
    if (username) {
      const routeList = [
        ...extractNames(constantRoute),
        ...(userStore.userInfos.routes as any[]),
        ...extractNames(anyRoute),
      ];
      if (routeList.includes(to.name)) {
        return next();
      } else {
        return next({ path: "/404" });
      }
    }

    // 获取用户信息
    try {
      await userStore.userInfo();
      return next({
        ...to,
      });
    } catch (error) {
      // 处理令牌过期
      await userStore.loginOut();
      return next({ path: "/login", query: { redirect: to.path } });
    }
  }

  // 情况2：用户未登录只能访问login,其余路由不能访问
  if (to.path === "/login") {
    return next();
  }

  // 对于其他路由，重定向到登录页面，并存储意图访问的路由
  next({ path: "/login", query: { redirect: to.path } });
});

router.afterEach(() => {
  close();
});
