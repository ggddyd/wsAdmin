export interface userInfoInterface {
  username: string;
  checkedRoles: string[];
  id: number;
}

export interface allRoleInterface {
  id: number;
  roleName: string;
}
export type allRoleListInterface = allRoleInterface[];
