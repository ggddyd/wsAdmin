interface cityOnlineNumType {
  name: string;
  value: number;
  areas?: string[];
}
export interface mapOptionInterface {
  num?: number;
  key?: string;
  cityOnlineNum?: cityOnlineNumType[];
}

export type mapDataType = mapOptionInterface[];
