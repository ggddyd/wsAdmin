enum API {
  //用户枚举地址:
  //获取全部已有用户账号信息
  ALLUSER_URL = "/admin/acl/user/",
  //添加一个新的用户账号
  ADDUSER_URL = "/admin/acl/user/save",
  //更新已有的用户账号
  UPDATEUSER_URL = "/admin/acl/user/update",
  //获取全部职位,当前账号拥有的职位接口
  ALLROLEURL = "/admin/acl/user/toAssign/",
  //给已有的用户分配角色接口
  SETROLE_URL = "/admin/acl/user/doAssignRole",
  //删除某一个账号
  DELETEUSER_URL = "/admin/acl/user/remove/",
  //批量删除的接口
  DELETEALLUSER_URL = "/admin/acl/user/batchRemove",

  //角色枚举地址:
  //获取全部的职位接口
  ALLROLE_URL = "/admin/acl/role/",
  //新增岗位的接口地址
  ADDROLE_URL = "/admin/acl/role/save",
  //更新已有的职位
  UPDATEROLE_URL = "/admin/acl/role/update",
  //获取全部的菜单与按钮的数据
  ALLPERMISSTION = "/admin/acl/permission/toAssign/",
  //给相应的职位分配权限
  SETPERMISTION_URL = "/admin/acl/permission/doAssign/?",
  //删除已有的职位
  REMOVEROLE_URL = "/admin/acl/role/remove/",

  //菜单枚举地址:
  //获取全部菜单与按钮的标识数据
  ALLPERMISSTION_URL = "/admin/acl/permission",
  //给某一级菜单新增一个子菜单
  ADDMENU_URL = "/admin/acl/permission/save",
  //更新某一个已有的菜单
  UPDATE_URL = "/admin/acl/permission/update",
  //删除已有的菜单
  DELETEMENU_URL = "/admin/acl/permission/remove/",
}
export default API;
