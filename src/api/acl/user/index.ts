//用户管理模块的接口
import request from "@/service/request";
import type {
  UserResponseData,
  User,
  AllRoleResponseData,
  SetRoleData,
} from "./user";
import API from "../api";

export default {
  //获取用户账号信息的接口
  reqUserInfo: (page: number, limit: number, username: string) =>
    request.get<UserResponseData>(
      API.ALLUSER_URL + `${page}/${limit}/?username=${username}`
    ),
  //添加用户与更新已有用户的接口
  reqAddOrUpdateUser: (data: User) => {
    //携带参数有ID更新
    if (data.id) {
      return request.put<any>(API.UPDATEUSER_URL, data);
    } else {
      return request.post<any>(API.ADDUSER_URL, data);
    }
  },
  //获取全部职位以及包含当前用户的已有的职位
  reqAllRole: (userId: number) =>
    request.get<AllRoleResponseData>(API.ALLROLEURL + userId),
  //分配职位
  reqSetUserRole: (data: SetRoleData) =>
    request.post<any>(API.SETROLE_URL, data),
  //删除某一个账号的信息
  reqRemoveUser: (userId: number) =>
    request.delete<any>(API.DELETEUSER_URL + userId),
  //批量删除的接口
  reqSelectUser: (idList: number[]) =>
    request.delete(API.DELETEALLUSER_URL, { data: idList }),
};
