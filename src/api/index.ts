import user from "./user/index.ts";
import aclUser from "./acl/user/index.ts";
// import aclRole from "./acl/role/index.ts"
export default {
  user,
  aclUser,
};
