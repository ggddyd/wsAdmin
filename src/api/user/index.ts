import request from "@/service/request";
import {
  loginFormData,
  loginResponseData,
  userInfoReponseData,
  loginOutInterface,
} from "./type";
import API from "./api";

export default {
  reqLogin: (data: loginFormData) =>
    request.post<loginResponseData>(API.LOGIN_URL, data),
  reqUserInfo: () => request.get<userInfoReponseData>(API.USERINFO_URL),
  reqLoginout: () => request.post<loginOutInterface>(API.LOGINOUT_URL),
};
