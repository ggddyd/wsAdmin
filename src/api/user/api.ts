enum API {
  LOGIN_URL = "/admin/acl/index/login",
  LOGINOUT_URL = "/admin/acl/index/logout",
  USERINFO_URL = "/admin/acl/index/info",
}

export default API;
