import type {
  AxiosError,
  InternalAxiosRequestConfig,
  AxiosResponse,
} from "axios";
import { ElMessage } from "element-plus";
import service from "./service";
import { statusMessages, HttpStatusCode } from "./error";
import StoragePlugin from "@/plugins/storagePlugin";
const storagePlugin = new StoragePlugin();

service.interceptors.request.use(
  (config: InternalAxiosRequestConfig<any>) => {
    const token: string | null = storagePlugin.get("TOKEN");
    // 给请求头设置token
    if (token) {
      config.headers!.token = token;
    }
    return config;
  },
  (error: AxiosError) => {
    ElMessage.error(error.message);
    return Promise.reject(error);
  }
);

/* 响应拦截器 */
service.interceptors.response.use(
  (response: AxiosResponse) => {
    // 将组件用的数据返回
    return response.data;
  },
  (error: AxiosError) => {
    // 处理 HTTP 网络错误
    const status = error.response?.status;
    const message = statusMessages[status as HttpStatusCode] || "token令牌失效";
    ElMessage.error(message);
  }
);

export default service;
