// 定义HTTP状态码的枚举
enum HttpStatusCode {
  Unauthorized = 401,
  Forbidden = 403,
  NotFound = 404,
  InternalServerError = 500,
  BadGateway = 502,
}

// 定义状态码对应的错误信息
const statusMessages: Record<HttpStatusCode, string> = {
  [HttpStatusCode.Unauthorized]: "token失效，请重新登录",
  [HttpStatusCode.Forbidden]: "没有权限，请获取权限后登录",
  [HttpStatusCode.NotFound]: "页面不存在",
  [HttpStatusCode.InternalServerError]: "服务器故障",
  [HttpStatusCode.BadGateway]: "数据库查询错误",
};

export { statusMessages, HttpStatusCode };
