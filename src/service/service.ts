import axios from "axios";
import type { AxiosInstance } from "axios";
const service: AxiosInstance = axios.create({
  baseURL: import.meta.env.VITE_APP_BASE_API,
  timeout: 5000,
});
export default service;
