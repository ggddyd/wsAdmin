export enum TimeOfDay {
  Morning = "早上",
  Forenoon = "上午",
  Afternoon = "下午",
  Evening = "晚上",
}
