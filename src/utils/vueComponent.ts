/*
 * @Description:
 * @Author: Wang Su
 * @Date: 2023-11-23 11:03:12
 * @LastEditors: Wang Su
 * @LastEditTime: 2023-11-23 11:03:28
 */

import SvgIcon from "@/components/SvgIcon/index.vue"; //buton //权限按钮
//引入全部图标
import * as ElementPlusIconsVue from "@element-plus/icons-vue";
export default {
  install(app: any) {
    app.component("SvgIcon", SvgIcon);
    for (const [key, component] of Object.entries(ElementPlusIconsVue)) {
      app.component(key, component);
    }
  },
};
