//账号校验
const validateUsername = (
  rule: any,
  value: string,
  callback: any,
  ruleForm: any
) => {
  if (value === "") {
    callback(new Error(rule.message || "请输入账号！"));
  } else {
    const reg = /^[A-Za-z0-9_]+$/;
    if (reg.test(value) && value != "") {
      callback();
    } else {
      callback(new Error(rule.message || "只支持数字，字母，_"));
      ruleForm.username = "";
    }
  }
};

//密码校验
const validatePassword = (
  rule: any,
  value: string,
  callback: any,
  ruleForm: any
) => {
  if (value === "") {
    callback(new Error(rule.message || "请输入密码！"));
  } else {
    const reg = /^[A-Za-z0-9_]+$/;
    if (reg.test(value) && value != "") {
      callback();
    } else {
      callback(new Error(rule.message || "只支持数字，字母，_"));
      ruleForm.password = "";
    }
  }
};

export { validateUsername, validatePassword };
