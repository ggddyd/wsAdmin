import { TimeOfDay } from "./type/time.d";
const timeOfDay = (): TimeOfDay => {
  const currentHour = new Date().getHours();
  switch (true) {
    case currentHour < 12:
      return TimeOfDay.Morning;
    case currentHour < 14:
      return TimeOfDay.Forenoon;
    case currentHour < 18:
      return TimeOfDay.Afternoon;
    default:
      return TimeOfDay.Evening;
  }
};

export { timeOfDay };
