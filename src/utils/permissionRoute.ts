function extractNames(routes: any[]): string[] {
  const namesArray: string[] = [];

  function recursiveExtract(route: any) {
    if (route.name) {
      namesArray.push(route.name);
    }

    if (route.children && route.children.length > 0) {
      for (const childRoute of route.children) {
        recursiveExtract(childRoute);
      }
    }
  }

  for (const route of routes) {
    recursiveExtract(route);
  }

  return namesArray;
}

export { extractNames };
