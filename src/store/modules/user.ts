//创建用户相关的小仓库
import { defineStore } from "pinia";
import { Names } from "../store.name";
import api from "@/api/index.ts";
import type { userInfoType } from "@/views/login/type";
import type { userStateInterface, userType } from "./types/user";
import router from "@/router";
//引入路由菜单
import { constantRoute, asycRoute, anyRoute } from "@/router/route";
import StoragePlugin from "@/plugins/storagePlugin";

import cloneDeep from "lodash/cloneDeep";
const storagePlugin = new StoragePlugin();
//用于过滤当前用户需要展示的异步路由
const filterAsyncRoutes = (asycRoute: any, routes: any) => {
  return asycRoute.filter((item: any) => {
    if (routes.includes(item.name)) {
      if (item.children && item.children.length > 0) {
        item.children = filterAsyncRoutes(item.children, routes);
      }
      return true;
    }
  });
};
const userInfoStore = defineStore(Names.User, {
  //小仓库
  state: (): userStateInterface => {
    return {
      //生成菜单
      menuRote: [],
      //用户信息
      userInfos: {},
    };
  },
  //处理逻辑的地方(异步 | 同步)
  actions: {
    //请求登录接口的方法
    async userLogin(ruleForm: userInfoType) {
      const res = await api.user.reqLogin(ruleForm);
      if (res.code === 200) {
        storagePlugin.set("TOKEN", res.data as string);
        return "ok";
      } else {
        return Promise.reject(new Error(res.message as string));
      }
    },
    //退出用户登录信息
    async loginOut() {
      const res = await api.user.reqLoginout();
      if (res.code == 200) {
        storagePlugin.remove("TOKEN");
        storagePlugin.remove("userInfo");
        this.userInfos = {};
        return "ok";
      } else {
        return Promise.reject(new Error(res.message));
      }
    },
    //请求用户信息
    async userInfo() {
      const res = await api.user.reqUserInfo();
      if (res.code === 200) {
        this.userInfos = res.data as userType;
        storagePlugin.set("userInfo", res.data as userType);
        const asyncRoutes = filterAsyncRoutes(
          cloneDeep(asycRoute),
          res.data.routes
        );
        this.menuRote = [...constantRoute, ...asyncRoutes, ...anyRoute];
        //需要把动态路由注册进来
        [...asyncRoutes, ...anyRoute].forEach((item) => {
          router.addRoute(item);
        });
        return "ok";
        !1;
      } else {
        return Promise.reject(new Error(res.message));
      }
    },
  },
  //计算属性
  getters: {},
});

export default userInfoStore;
