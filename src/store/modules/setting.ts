//创建用户相关的小仓库
import { defineStore } from "pinia";
import { Names } from "../store.name";
import { settingInterface } from "./types/setting.d";
const settingStore = defineStore(Names.SETING, {
  //小仓库
  state: (): settingInterface => {
    return {
      fold: false,
      refresh: false,
    };
  },
  //处理逻辑的地方(异步 | 同步)
  actions: {},
  //计算属性
  getters: {},
});

export default settingStore;
