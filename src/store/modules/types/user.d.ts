import type { RouteRecordRaw } from "vue-router";

export interface userStateInterface {
  menuRote: RouteRecordRaw[];
  userInfos: userType;
}
interface userInterface {
  routes: string[];
  buttons: string[];
  roles: string[];
  name: string;
  avatar: string;
}
export type userType = Partial<userInterface>;
