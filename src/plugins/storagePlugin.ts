interface StorageOptions {
  prefix?: string;
}

class StoragePlugin {
  private prefix: string;

  constructor(options: StorageOptions = {}) {
    this.prefix = options.prefix || "";
  }

  set(key: string, value: any) {
    localStorage.setItem(this.prefix + key, JSON.stringify(value));
  }

  get(key: string, defaultValue?: any) {
    const value = localStorage.getItem(this.prefix + key);
    if (value !== null) {
      return JSON.parse(value);
    }
    return defaultValue;
  }

  remove(key: string) {
    localStorage.removeItem(this.prefix + key);
  }

  clear() {
    localStorage.clear();
  }
}
export default StoragePlugin;
