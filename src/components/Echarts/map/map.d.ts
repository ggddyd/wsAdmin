export interface geoCoordMapInterface {
  [x: string]: number[];
}
interface toolInterface {
  name: string;
  value: number;
  areas: string[];
}
export type toolTipDataInterface = toolInterface[];

interface dataInterface {
  name: string;
  value: number;
}
export type dataType = dataInterface[];
