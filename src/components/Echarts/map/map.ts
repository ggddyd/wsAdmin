import { toolTipDataInterface, dataType } from "./map.d";
const getMapOption = (
  mapFeatures: any,
  data: dataType,
  toolTipData: toolTipDataInterface,
  geoCoordMaps: any,
  mapName: string,
  img2: string
) => {
  const convertData = function (data: dataType) {
    const res = [];
    for (let i = 0; i < data.length; i++) {
      const geoCoord = geoCoordMaps[data[i].name];
      if (geoCoord) {
        res.push({
          name: data[i].name,
          value: geoCoord.concat(data[i].value),
        });
      }
    }
    return res;
  };
  // 柱状体的主干
  const lineData = () => {
    return toolTipData.map((item) => {
      return {
        coords: [
          geoCoordMaps[item.name],
          [geoCoordMaps[item.name][0], geoCoordMaps[item.name][1] + 0.6],
        ],
      };
    });
  };
  // 柱状体的顶部
  const scatterData = () => {
    return toolTipData.map((item) => {
      return [
        geoCoordMaps[item.name][0],
        geoCoordMaps[item.name][1] + 0.7,
        item,
      ];
    });
  };

  mapFeatures.forEach(function (v: any) {
    // 地区名称
    const name = v.properties.name;
    // 地区经纬度
    geoCoordMaps[name] = v.properties.center;
  });

  const option = {
    backgroundColor: "rgb(237, 244, 245)",
    tooltip: {
      trigger: "none",
      formatter: function (params: any) {
        if (typeof params.value[2] == "undefined") {
          let toolTiphtml = "";
          for (let i = 0; i < toolTipData.length; i++) {
            if (params.name == toolTipData[i].name) {
              toolTiphtml += toolTipData[i].name + "：" + toolTipData[i].value;
            }
          }
          return toolTiphtml;
        } else {
          let toolTiphtml = "";
          for (let i = 0; i < toolTipData.length; i++) {
            if (params.name == toolTipData[i].name) {
              toolTiphtml += toolTipData[i].name + "：" + toolTipData[i].value;
            }
          }

          return toolTiphtml;
        }
      },
      backgroundColor: "rgb(237, 244, 245)",
      borderColor: "#333",
      padding: [5, 10],
      textStyle: {
        color: "#333",
        fontSize: "16",
      },
    },
    geo: [
      {
        layoutCenter: ["50%", "50%"], //位置
        layoutSize: "150%", //大小
        show: true,
        map: mapName,
        roam: false,
        zoom: 0.65,
        aspectScale: 1,
        label: {
          normal: {
            show: false,
            textStyle: {
              color: "#fff",
            },
          },
          emphasis: {
            show: true,
            textStyle: {
              color: "#fff",
            },
          },
        },
        itemStyle: {
          normal: {
            areaColor: {
              type: "linear",
              x: 1200,
              y: 0,
              x2: 0,
              y2: 0,
              colorStops: [
                {
                  offset: 0,
                  color: "rgba(3,27,78,0.75)", // 0% 处的颜色
                },
                {
                  offset: 1,
                  color: "rgba(58,149,253,0.75)", // 50% 处的颜色
                },
              ],
              global: true, // 缺省为 false
            },
            borderColor: "#c0f3fb",
            borderWidth: 1,
            shadowColor: "#8cd3ef",
            shadowOffsetY: 10,
            shadowBlur: 120,
          },
          emphasis: {
            areaColor: "rgba(0,254,233,0.6)",
            // borderWidth: 0
          },
        },
      },
      {
        type: "map",
        map: mapName,
        zlevel: -1,
        aspectScale: 1,
        zoom: 0.65,
        layoutCenter: ["50%", "51%"],
        layoutSize: "150%",
        roam: false,
        silent: true,
        itemStyle: {
          normal: {
            borderWidth: 0,
            shadowColor: "rgba(172, 122, 255,0.5)",
            shadowOffsetY: 5,
            shadowBlur: 15,
            areaColor: "rgb(237, 244, 245)",
          },
        },
      },
      {
        type: "map",
        map: mapName,
        zlevel: -2,
        aspectScale: 1,
        zoom: 0.65,
        layoutCenter: ["50%", "52%"],
        layoutSize: "150%",
        roam: false,
        silent: true,
        itemStyle: {
          normal: {
            borderWidth: 1,
            borderColor: "rgba(58,149,253,0.6)",
            shadowColor: "rgba(65, 214, 255,1)",
            shadowOffsetY: 5,
            shadowBlur: 15,
            areaColor: "transpercent",
          },
        },
      },
      {
        type: "map",
        map: mapName,
        zlevel: -3,
        aspectScale: 1,
        zoom: 0.65,
        layoutCenter: ["50%", "53%"],
        layoutSize: "150%",
        roam: false,
        silent: true,
        itemStyle: {
          normal: {
            borderWidth: 1,
            // borderColor: "rgba(11, 43, 97,0.8)",
            borderColor: "rgba(58,149,253,0.4)",
            shadowColor: "rgba(58,149,253,1)",
            shadowOffsetY: 15,
            shadowBlur: 10,
            areaColor: "transpercent",
          },
        },
      },
      {
        type: "map",
        map: mapName,
        zlevel: -4,
        aspectScale: 1,
        zoom: 0.65,
        layoutCenter: ["50%", "54%"],
        layoutSize: "150%",
        roam: false,
        silent: true,
        itemStyle: {
          normal: {
            borderWidth: 5,
            borderColor: "rgba(5,9,57,0.8)",
            shadowColor: "rgba(29, 111, 165,0.8)",
            shadowOffsetY: 15,
            shadowBlur: 10,
            areaColor: "rgba(5,21,35,0.1)",
          },
        },
      },
    ],
    series: [
      {
        type: "map",
        map: mapName,
        geoIndex: 0,
        zoom: 0.65,
        showLegendSymbol: true,
        roam: true,
        label: {
          normal: {
            show: true,
            textStyle: {
              color: "#fff",
              fontSize: "120%",
            },
          },
          emphasis: {
            // show: false,
          },
        },
        itemStyle: {
          normal: {
            areaColor: {
              type: "linear",
              x: 1200,
              y: 0,
              x2: 0,
              y2: 0,
              colorStops: [
                {
                  offset: 0,
                  color: "rgba(3,27,78,0.75)", // 0% 处的颜色
                },
                {
                  offset: 1,
                  color: "rgba(58,149,253,0.75)", // 50% 处的颜色
                },
              ],
              global: true, // 缺省为 false
            },
            borderColor: "#fff",
            borderWidth: 0.2,
          },
        },
        layoutCenter: ["50%", "50%"],
        layoutSize: "150%",
        animation: false,
        markPoint: {
          symbol: "none",
        },
        data: data,
      },
      //柱状体的主干
      {
        type: "lines",
        zlevel: 5,
        effect: {
          show: false,
          symbolSize: 5, // 图标大小
        },
        lineStyle: {
          width: 6, // 尾迹线条宽度
          color: "rgba(249, 105, 13, .6)",
          opacity: 1, // 尾迹线条透明度
          curveness: 0, // 尾迹线条曲直度
        },
        label: {
          show: 0,
          position: "end",
          formatter: "245",
        },
        silent: true,
        data: lineData(),
      },
      // 柱状体的顶部
      {
        type: "scatter",
        coordinateSystem: "geo",
        geoIndex: 0,
        zlevel: 5,
        label: {
          normal: {
            show: true,
            formatter: function (params: any) {
              const name = params.data[2].name;
              const value = params.data[2].value;
              const text = `{tline|${name}} : {fline|${value}}台`;
              return text;
            },
            color: "#fff",
            rich: {
              fline: {
                color: "#fff",
                fontSize: 14,
                fontWeight: 600,
              },
              tline: {
                color: "#ABF8FF",
                fontSize: 12,
              },
            },
          },
          emphasis: {
            show: true,
          },
        },
        itemStyle: {
          color: "#00FFF6",
          opacity: 1,
        },
        symbol: img2,
        symbolSize: [110, 60],
        symbolOffset: [0, -20],
        z: 999,
        data: scatterData(),
      },
      {
        name: "Top 5",
        type: "effectScatter",
        coordinateSystem: "geo",
        data: convertData(toolTipData),
        showEffectOn: "render",
        rippleEffect: {
          scale: 5,
          brushType: "stroke",
        },
        label: {
          normal: {
            formatter: "{b}",
            position: "bottom",
            show: false,
            color: "#fff",
            distance: 10,
          },
        },
        symbol: "circle",
        symbolSize: [20, 10],
        itemStyle: {
          normal: {
            color: "#16ffff",
            shadowBlur: 10,
            shadowColor: "#16ffff",
          },
          opacity: 1,
        },
        zlevel: 4,
      },
    ],
  };

  return option;
};
export default getMapOption;
