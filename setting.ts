/*
 * @Description:
 * @Author: Wang Su
 * @Date: 2023-11-24 16:03:41
 * @LastEditors: Wang Su
 * @LastEditTime: 2023-11-24 16:46:56
 */
import type { settingType } from "./setting.d";
const setConfig: settingType = {
  title: "国网大屏展示平台",
  logoFlag: true,
};
export default setConfig;
