interface setType {
  title: string;
  logoFlag: boolean;
}

export type settingType = Partial<setType>;
